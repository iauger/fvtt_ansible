# Foundry VTT via Ansible

A basic set of configuration scripts for setting up Foundry VTT on a remote linux host using Ansible.

## Usage

First, [install Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html).

Then, clone the repo. Switch to the directory it's in, then run the following, substituting the IP address or FQDN of the target server.

    ansible-playbook -i <target server> -K playbook.yml

You will be prompted to input the sudo password for the remote machine, a timed url for the Foundry download, and the user Foundry should install under.  Note that there is no validation on the user you put in, and I'm pretty sure the bad things will happen if you put in a user that doesn't exist.  A few minutes later, you should have a Foundry server up and running.

Note that this is a no-frills server -- no reverse proxy, no SSL, running on the default port.  A systemd service file is used to start Foundry and make it persist on reboot.

## To-Dos

- Add user account validation (e.g. is the account you're trying to install it as there, possibly create it if not)
- Add frills
	- Install nginx and populate a config
	- Run certbot to pull SSL certs
- Tie into Terraform to roll out automated infrastructure setup and make setup a one-touch process that spins up appropriate cloud resources at the same time.
- Tidy up the playbook by breaking tasks out into roles.

## Contact Me

This is provided by @magichateball on the Foundry Discord, but odds are good you knew that already if you found this link.
